FROM node:14.15.0-alpine3.12
ENTRYPOINT ["firebase"]
CMD ["emulators:start"]

ENV FIREBASE_TOOLS_VERSION=8.17.0
RUN yarn global add firebase-tools@${FIREBASE_TOOLS_VERSION} && \
    yarn cache clean && \
    firebase -V && \
    mkdir $HOME/.cache

COPY firebase.json /
COPY firebaserc /.firebaserc
